#version 330 compatibility
in vec4 myColor;
void
main()
{
	gl_FragColor = myColor;
}