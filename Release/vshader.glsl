#version 330 compatibility
layout (location = 0) in vec4 vPosition;
layout (location = 1) in vec3 colors;
layout (location = 2) in vec3 offset;
layout (location = 3) in vec3 scale;
uniform mat4 proj;
uniform mat4 view;
out vec4 myColor;
void main()
{
    vec3 pos;
    pos.x = vPosition.x * scale.x;
    pos.y = vPosition.y * scale.y;
    pos.z = vPosition.z * scale.z;
    gl_Position = proj * view * gl_ModelViewProjectionMatrix * (pos + vec4(offset, 1));
    myColor = vec4(colors, 1);
}