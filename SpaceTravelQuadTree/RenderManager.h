#pragma once

#include "glm/glm.hpp"


//#define ROWS 100  // Number of rows of asteroids.
//#define COLUMNS 100 // Number of columns of asteroids.
//#define FILL_PROBABILITY 100 // Percentage probability that a particular row-column slot will be 
//#define SPHERE_VERTEX_COUNT 288
//#define SPHERE_SIZE 5.0f
//// vertex counting for where everything goes in the global array
//// fixed number of vertices for cone and sphere
//#define CONE_VERTEX_COUNT 12
//#define LINE_VERTEX_COUNT 2
//
//
//// initial indices where data starts getting drawn for different data types
//extern int cone_index = 0;
//extern int line_index = cone_index + CONE_VERTEX_COUNT;
//extern int sphere_index = line_index + LINE_VERTEX_COUNT;


class RenderManager
{
public:
	RenderManager();
	~RenderManager();

	void render();
};

