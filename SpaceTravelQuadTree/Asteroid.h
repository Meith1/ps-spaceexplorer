
#ifndef Asteroid_13389437
#define Asteroid_13389437
#include <glm/glm.hpp>

#define SPHERE_VERTEX_COUNT 288
#define SPHERE_SIZE 5.0f

struct Asteroid {
	float radius;
	float centerX, centerY, centerZ;
	float color[3];
};


#endif