#pragma once
#include <GL/glew.h>
#include "GL/glfw3.h"

#include <iostream>

class InputManager
{
public:
	InputManager();
	~InputManager();

	bool keys[1024];
	bool keysProcessed[1024];
	void handleInputs();
};

	

