#include "RenderManager.h"
#include "Asteroid.h"
#include <GL/glew.h>
#include <glm/glm.hpp>




RenderManager::RenderManager()
{
}


RenderManager::~RenderManager()
{
}

//void RenderManager::render()
//{
//	int i, j;
//	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//
//	//// Use the buffer and shader for each circle.
//	//glUseProgram(myShaderProgram);
//	//glBindBuffer(GL_ARRAY_BUFFER, myBuffer);
//
//	//// Initialize the vertex position attribute from the vertex shader.
//	//GLuint loc = glGetAttribLocation(myShaderProgram, "vPosition");
//	//glEnableVertexAttribArray(loc);
//	//glVertexAttribPointer(loc, 3, GL_FLOAT, GL_FALSE, 0, 0);
//	//// copy over global points array in case anything has changed
//	//// very slow when points is just HUGE
//	//glBindBuffer(GL_ARRAY_BUFFER, myBuffer);
//	//glBufferData(GL_ARRAY_BUFFER, sizeof(points), points, GL_STATIC_DRAW);
//
//	// Begin left viewport.
////	glViewport(0, height / 1.25, width / 4.0, height / 4.0);
//	glLoadIdentity();
//
//	// Write text in isolated (i.e., before gluLookAt) translate block.
//	// DOES NOT WORK WITHOUT GLUT 
//	//glPushMatrix();
//	//glColor3f(1.0, 1.0, 1.0);
//	//glRasterPos3f(5.0, 25.0, -30.0);
//	//if (isFrustumCulled) writeBitmapString((void*)font, "Frustum culling on!");
//	//else writeBitmapString((void*)font, "Frustum culling off!");
//	//glColor3f(1.0, 0.0, 0.0);
//	//glRasterPos3f(-28.0, 25.0, -30.0);
//	//if (isCollision) writeBitmapString((void*)font, "Cannot - will crash!");
//	//glPopMatrix();
//
//	// Fixed camera 
//	// lookAt(0.0, 10.0, 20.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
//
//	// Top Camera
//	//lookAt(xVal - 10 * sin((PI / 180.0) * angle),
//	//	90.0,
//	//	zVal - 10 * cos((PI / 180.0) * angle),
//	//	xVal - 11 * sin((PI / 180.0) * angle),
//	//	0.0,
//	//	zVal - 11 * cos((PI / 180.0) * angle),
//	//	0.0,
//	//	1.0,
//	//	0.0);
//
//	//int xdist, zdist, dist;
//	//if (!isFrustumCulled)
//	//{
//	//	// Draw all the asteroids in arrayAsteroids.
//	//	for (i = 0; i<ROWS; i++)
//	//		for (j = 0; j<COLUMNS; j++)
//	//			arrayAsteroids[i][j].draw();
//	//}
//
//	else
//	{
//		// Draw only asteroids in leaf squares of the quadtree that intersect the fixed frustum
//		// with apex at the origin.
//		// asteroidsQuadtree.drawAsteroids(-5.0, -5.0, -250.0, -250.0, 250.0, -250.0, 5.0, -5.0);
//
//		for (i = 0; i<ROWS; i++)
//		{
//			for (j = 0; j<COLUMNS; j++)
//			{
//				xdist = arrayAsteroids[i][j].getCenterX() - xVal;
//				zdist = arrayAsteroids[i][j].getCenterZ() - zVal;
//
//				dist = (xdist*xdist) + (zdist*zdist);
//
//				/* std::cout << xdist;
//				std::cout << zdist;
//				std::cout << dist;*/
//
//				if (dist < 5000)
//					arrayAsteroids[i][j].draw();
//			}
//		}
//	}
//
//	// off is white spaceship and on it red
//	if (isFrustumCulled)
//		glColor3f(1.0, 0.0, 0.0);
//	else
//		glColor3f(1.0, 1.0, 1.0);
//
//	// spacecraft moves and so we translate/rotate according to the movement
//	glPushMatrix();
//	glTranslatef(xVal, 0, zVal);
//	glRotatef(angle, 0.0, 1.0, 0.0);
//
//	glPushMatrix();
//	glRotatef(-90.0, 1.0, 0.0, 0.0); // To make the spacecraft point down the $z$-axis initially.
//
//	// Turn on wireframe mode
//	glPolygonMode(GL_FRONT, GL_LINE);
//	glPolygonMode(GL_BACK, GL_LINE);
//	glDrawArrays(GL_TRIANGLE_FAN, cone_index, CONE_VERTEX_COUNT);
//	// Turn off wireframe mode
//	glPolygonMode(GL_FRONT, GL_FILL);
//	glPolygonMode(GL_BACK, GL_FILL);
//	glPopMatrix();
//	// End left viewport.
//
//	// Begin right viewport.
//	glViewport(0, 0, width, height);
//	glLoadIdentity();
//
//	// Write text in isolated (i.e., before gluLookAt) translate block.
//	// DOES NOT WORK WITHOUT GLUT
//	//glPushMatrix();
//	//glColor3f(1.0, 1.0, 1.0);
//	//glRasterPos3f(5.0, 25.0, -30.0);
//	//if (isFrustumCulled)  writeBitmapString((void*)font, "Frustum culling on.");
//	//else writeBitmapString((void*)font, "Frustum culling off.");
//	//glColor3f(1.0, 0.0, 0.0);
//	//glRasterPos3f(-28.0, 25.0, -30.0);
//	//if (isCollision)  writeBitmapString((void*)font, "Cannot - will crash!");
//	//glPopMatrix();
//
//	// draw the line in the middle to separate the two viewports
//	/*  glPushMatrix();
//	glTranslatef(-6, 0, 0);
//	glColor3f(1.0, 1.0, 1.0);
//	glLineWidth(2.0);
//	glDrawArrays(GL_LINE_STRIP, line_index, LINE_VERTEX_COUNT);
//	glLineWidth(1.0);
//	glPopMatrix();*/
//
//	// Locate the camera at the tip of the cone and pointing in the direction of the cone.
//	lookAt(xVal - 10 * sin((PI / 180.0) * angle),
//		0.0,
//		zVal - 10 * cos((PI / 180.0) * angle),
//		xVal - 11 * sin((PI / 180.0) * angle),
//		0.0,
//		zVal - 11 * cos((PI / 180.0) * angle),
//		0.0,
//		1.0,
//		0.0);
//
//	if (!isFrustumCulled)
//	{
//		// Draw all the asteroids in arrayAsteroids.
//		for (i = 0; i<ROWS; i++)
//			for (j = 0; j<COLUMNS; j++)
//				arrayAsteroids[i][j].draw();
//	}
//	else
//	{
//		// Draw only asteroids in leaf squares of the quadtree that intersect the frustum
//		// "carried" by the spacecraft with apex at its tip and oriented with its axis
//		// along the spacecraft's axis.
//		/*  asteroidsQuadtree.drawAsteroids(xVal - 7.072 * sin((PI / 180.0) * (45.0 + angle)),
//		zVal - 7.072 * cos((PI / 180.0) * (45.0 + angle)),
//		xVal - 353.6 * sin((PI / 180.0) * (45.0 + angle)),
//		zVal - 353.6 * cos((PI / 180.0) * (45.0 + angle)),
//		xVal + 353.6 * sin((PI / 180.0) * (45.0 - angle)),
//		zVal - 353.6 * cos((PI / 180.0) * (45.0 - angle)),
//		xVal + 7.072 * sin((PI / 180.0) * (45.0 - angle)),
//		zVal - 7.072 * cos((PI / 180.0) * (45.0 - angle))
//		);*/
//
//		for (i = 0; i<ROWS; i++)
//		{
//			for (j = 0; j<COLUMNS; j++)
//			{
//				xdist = arrayAsteroids[i][j].getCenterX() - xVal;
//				zdist = arrayAsteroids[i][j].getCenterZ() - zVal;
//
//				dist = (xdist*xdist) + (zdist*zdist);
//
//				/* std::cout << xdist;
//				std::cout << zdist;
//				std::cout << dist;*/
//
//				if (dist < 5000)
//					arrayAsteroids[i][j].draw();
//			}
//		}
//	}
//	// End right viewport.
//}