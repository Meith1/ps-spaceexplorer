// A smaller and faster running version of the spaceTravelFrustumCulled.cpp program
#include <GL/glew.h>
#include <malloc.h>
#include <cstdlib>
#include <ctime> 
#include <cmath>
#include <iostream>
#include <fstream>
#include <GL/glfw3.h>
#include <glm/glm.hpp>
#include "glm\gtc\matrix_transform.hpp"
#include <list>
#include <vector>
#include "intersectionDetectionRoutines.h"
#include "Asteroid.h"
#include "InputManager.h"

using namespace std;

#define ROWS 1000  // Number of rows of asteroids.
#define COLUMNS 1000 // Number of columns of asteroids.
#define FILL_PROBABILITY 100 // Percentage probability that a particular row-column slot will be 
                             // filled with an asteroid. It should be an integer between 0 and 100.
#define WINDOW_X 1024
#define WINDOW_Y 768

// Globals.
static int width, height; // Size of the OpenGL window.
static float angle = 0.0; // Angle of the spacecraft.
static float xVal = 0, zVal = 0; // Co-ordinates of the spacecraft.
static int isFrustumCulled = 0;
static int isCollision = 0; // Is there collision between the spacecraft and an asteroid?

//input manager
InputManager im;

// vertex counting for where everything goes in the global array
// fixed number of vertices for cone and sphere
#define CONE_VERTEX_COUNT 12
#define LINE_VERTEX_COUNT 2
// #define SPHERE_VERTEX_COUNT 288 // moved to Asteroids.h because asteroids draw themselves

// initial indices where data starts getting drawn for different data types
int cone_index = 0;
int line_index = cone_index + CONE_VERTEX_COUNT;
int sphere_index = line_index + LINE_VERTEX_COUNT;

// shader stuff
#define OTHER_ITEMS 2
#define LINE_ITEM 0
#define CONE_ITEM 1
glm::vec3 points[CONE_VERTEX_COUNT+LINE_VERTEX_COUNT+SPHERE_VERTEX_COUNT]; // addition of all rows/cols for asteroid vertices + spaceship vertices + line vertices  
glm::vec3 translations[ROWS*COLUMNS + OTHER_ITEMS];
glm::vec4 colors[ROWS*COLUMNS + OTHER_ITEMS];
glm::vec3 scales[ROWS*COLUMNS + OTHER_ITEMS];
GLuint  myShaderProgram;
GLuint InitShader(const char* vShaderFile, const char* fShaderFile);
GLuint	instancedBuffer;
GLuint	transBuffer;
GLuint	colorBuffer;
GLuint	scaleBuffer;
GLuint  projectionBuffer;
GLuint  viewBuffer;

// the asteroids and quad tree from the initial program
Asteroid arrayAsteroids[ROWS][COLUMNS]; // Global array of asteroids.

//static long font = (long)GLUT_BITMAP_8_BY_13; // Font selection.
// Routine to draw a bitmap character string.
// DOES NOT WORK WITHOUT GLUT
void writeBitmapString(void *font, char *string)
{  
  // char *c;
//   for (c = string; *c != '\0'; c++) glutBitmapCharacter(font, *c);
} 

// function obtained from tutorial at:
// http://www.freemancw.com/2012/06/opengl-cone-function/
// used in drawing a cone
glm::vec3  perp(const glm::vec3 &v) {
	float min = fabs(v.x);
	glm::vec3 cardinalAxis( 1, 0, 0 );

	if (fabs(v.y) < min) {
		min = fabs(v.y);
		cardinalAxis = glm::vec3( 0, 1, 0);
	}

	if (fabs(v.z) < min) {
		cardinalAxis = glm::vec3(0, 0, 1);
	}

	return glm::cross(v, cardinalAxis);
}

// simple function to save a lot of writing to set up vector values
void setVec3(glm::vec3 &vector, const float x, const float y, const float z) {
	vector.x = x;
	vector.y = y;
	vector.z = z;
}

void setVec4(glm::vec4 &vector, const float x, const float y, const float z, const float w) {
	vector.x = x;
	vector.y = y;
	vector.z = z;
	vector.w = w;
}

// function derived from tutorial at:
// http://www.freemancw.com/2012/06/opengl-cone-function/
void CreateCone(const glm::vec3 &d, const glm::vec3 &a,
	const float h, const float rd, const int n, int offset) {
	glm::vec3 c;
	c.x = a.x + (-d.x * h);
	c.y = a.y + (-d.y * h);
	c.z = a.z + (-d.z * h);
	glm::vec3 e0 = perp(d);
	glm::vec3 e1 = glm::cross(e0, d);
	float angInc = 360.0f / n * (PI / 180.0f);

	// the cone
	vector <glm::vec3> pts;
	for (int i = 0; i < n; ++i) {
		float rad = angInc * i;
		glm::vec3 p = c + (((e0 * cos(rad)) + (e1 * sin(rad))) * rd);
		pts.push_back(p);
	}

	// cone top
	int i = 0;
	int o = offset + i;
	setVec3(points[o], a.x, a.y, a.z);

	for (i = 1; i < n+1; ++i) {
		o = i + offset;
		setVec3(points[o], pts[i - 1].x, pts[i - 1].y, pts[i - 1].z);
	}

	o = i + offset;
	setVec3(points[o], pts[0].x, pts[0].y, pts[0].z);

	// original tutorial has cone bottom
	// not necessary for this app when cone is a spaceship!
}

// function derived from tutorial at:
// http://www.swiftless.com/tutorials/opengl/sphere.html
void CreateSphere(const float H, const float K, const float Z, const int offset) {
	int n;
	float a;
	float b;
	n = 0;
	const int space = 30;
	float x, y, z;
	float cosa, cosb, sina, sinb;
	float sinaSpace, sinbSpace, cosaSpace, cosbSpace;
	// back half offset is defined as (((90-space)/space)+1) * ((360-space)/space + 1) * 4
	int backOffset = (((90 - space) / space) + 1) * ((360 - space) / space + 1) * 4;
	
	// draw the front and back halves of the sphere
	for (b = 0; b <= 90 - space; b += space){
		for (a = 0; a <= 360 - space; a += space){
			// save some recalculating below for different points
			sina = sinf((a) / 180.0f * PI);
			sinb = sinf((b) / 180 * PI);
			cosa = cosf((a) / 180 * PI);
			cosb = cosf((b) / 180 * PI);
			sinaSpace = sinf((a + space) / 180 * PI);
			sinbSpace = sinf((b + space) / 180 * PI);
			cosaSpace = cosf((a + space) / 180 * PI);
			cosbSpace = cosf((b + space) / 180 * PI);

			// draw both the back and front. back is just -1*z different
			x = sina * sinb - H;
			y = cosa * sinb + K;
			z = cosb - Z;
			setVec3(points[n + offset], x, y, z);
			setVec3(points[n + offset+backOffset], x, y, -1 * z);
			n++;

			x = sina * sinbSpace - H;
			y = cosa * sinbSpace + K;
			z = cosbSpace - Z;
			setVec3(points[n + offset], x, y, z);
			setVec3(points[n + offset + backOffset], x, y, -1 * z);
			n++;

			x = sinaSpace * sinb - H;
			y = cosaSpace * sinb + K;
			z = cosb - Z;
			setVec3(points[n + offset], x, y, z);
			setVec3(points[n + offset + backOffset], x, y, -1 * z);
			n++;

			x = sinaSpace * sinbSpace - H;
			y = cosaSpace * sinbSpace + K;
			z = cosbSpace - Z;
			setVec3(points[n + offset], x, y, z);
			setVec3(points[n + offset + backOffset], x, y, -1 * z);
			n++;
		}
	}

}

// OpenGL window reshape routine.
void resize(GLFWwindow* window, int w, int h)
{
	glViewport(0, 0, (GLsizei)w, (GLsizei)h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);

	// Pass the size of the OpenGL window.
	width = w;
	height = h;
}

void createAsteroidField(const int fillProperty) {
	// create the asteroid field itself
	// Initialize global arrayAsteroids.
	int i;
	int j;
	for (i = 0; i < ROWS; i++) {
		for (j = 0; j < COLUMNS; j++)
			if (rand() % 100 < fillProperty)
			{
			// If rand()%100 >= FILL_PROBABILITY the default constructor asteroid remains in the slot which
			// indicates that there is no asteroid there because the default's radius is 0.
			// Position the asteroids depending on if there is an even or odd number of columns
			// so that the spacecraft faces the middle of the asteroid field.
			if (COLUMNS % 2) // Odd number of columns. 
			{
				arrayAsteroids[i][j].centerX = 30.0f*(-COLUMNS / 2 + j);
			}
			else // Even number of columns. 
			{
				arrayAsteroids[i][j].centerX = 15.0f + 30.0f*(-COLUMNS / 2 + j);
			}
			arrayAsteroids[i][j].centerY = (rand() % 3000) / 25.0f - 40;
			arrayAsteroids[i][j].centerZ = -40.0f - 30.0f*i;
			arrayAsteroids[i][j].radius = (rand() % 500) / 100.f;  // around 5.0 is largest
			arrayAsteroids[i][j].color[0] = (rand() % 256) / 256.0f;
			arrayAsteroids[i][j].color[1] = (rand() % 256) / 256.0f;
			arrayAsteroids[i][j].color[2] = (rand() % 256) / 256.0f;
			setVec3(translations[i*COLUMNS + j + OTHER_ITEMS], arrayAsteroids[i][j].centerX, arrayAsteroids[i][j].centerY,
				arrayAsteroids[i][j].centerZ);
			setVec4(colors[i*COLUMNS + j + OTHER_ITEMS], arrayAsteroids[i][j].color[0], arrayAsteroids[i][j].color[1], 
				arrayAsteroids[i][j].color[2], 1.0f);
			setVec3(scales[i*COLUMNS + j + OTHER_ITEMS], arrayAsteroids[i][j].radius, arrayAsteroids[i][j].radius,
				arrayAsteroids[i][j].radius);
			}
	}
			
}

void setFrustum(glm::mat4 &projection, float fovy, float aspect, float zNear, float zFar) {
	float tanForFovY = tan( fovy / 2.0f);

	// assume that the projection matrix is set to 0 before calling this function.

	projection[0][0] = 1.0f / (aspect * tanForFovY);
	projection[1][1] = 1.0f / (tanForFovY);
	projection[2][2] = -(zFar + zNear) / (zFar - zNear);
	projection[2][3] = -1.0f;
	projection[3][2] = -(2.0f * zFar * zNear) / (zFar - zNear);

}

// Initialization routine.
void setup(void) 
{
   // create the line for the middle of the screen
   setVec3(points[line_index], 0, -5, -6);
   setVec3(points[line_index + 1], 0, 5, -6);
   setVec3(translations[0], 3.9f, 0.0f, 0.0f );
   setVec3(scales[LINE_ITEM], 1.0f, 1.0f, 1.0f);
   setVec4(colors[LINE_ITEM], 1.0f, 1.0f, 1.0f, 1.0f);

   // create the cone for a spaceship
   glm::vec3 direction( 0, 1, 0 );
   glm::vec3 apex(0, 10, 0);
   CreateCone(direction, apex, 10, 5, 10, cone_index);
   setVec3(translations[CONE_ITEM], 0.0f, 0.0f, 0.0f);
   setVec3(scales[CONE_ITEM], 1.0f, 1.0f, 1.0f);
   setVec4(colors[CONE_ITEM], 0.8f, 0.0f, 0.6f, 1.0f);

   // create where the spheres are going in the field   
   int index = sphere_index;
   CreateSphere(0, 0, 0, index);
   
   // fill rate for asteroids is random out of 100%
   createAsteroidField(100);
    
   // initialize the graphics
   glEnable(GL_DEPTH_TEST);
   glClearColor (0.0, 0.0, 0.0, 0.0);
   glViewport(0, 0, (GLsizei)WINDOW_X, (GLsizei)WINDOW_Y);

   // Load shaders and use the resulting shader program
   GLuint program = InitShader("vshader.glsl", "fshader.glsl");
   myShaderProgram = program;
   glUseProgram(myShaderProgram);

   // Create a vertex array object
   GLuint vao;
   glGenVertexArrays(1, &vao);
   glBindVertexArray(vao);

   // Create and initialize a buffer object for each circle
   GLuint aBuffer;
   glGenBuffers(1, &aBuffer);
   instancedBuffer = aBuffer;
   glBindBuffer(GL_ARRAY_BUFFER, instancedBuffer);
   glBufferData(GL_ARRAY_BUFFER, sizeof(points), points, GL_STATIC_DRAW);

 
   // Initialize the vertex position attribute from the vertex shader
   GLuint loc = glGetAttribLocation(myShaderProgram, "vPosition");
   glEnableVertexAttribArray(loc);
   glVertexAttribPointer(loc, 3, GL_FLOAT, GL_FALSE, 0, 0);

   GLuint instanceVBO;
   glGenBuffers(1, &instanceVBO);
   transBuffer = instanceVBO;
   glBindBuffer(GL_ARRAY_BUFFER, instanceVBO);
   glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * ROWS*COLUMNS, translations, GL_STATIC_DRAW);
   glEnableVertexAttribArray(2);
   glBindBuffer(GL_ARRAY_BUFFER, instanceVBO);
   glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
   glVertexAttribDivisor(2, 1);

   glGenBuffers(1, &instanceVBO);
   scaleBuffer = instanceVBO;
   glBindBuffer(GL_ARRAY_BUFFER, instanceVBO);
   glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) *ROWS * COLUMNS, scales, GL_STATIC_DRAW);
   glEnableVertexAttribArray(3);
   glBindBuffer(GL_ARRAY_BUFFER, instanceVBO);
   glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
   glVertexAttribDivisor(3, 1);

   glGenBuffers(1, &instanceVBO);
   colorBuffer = instanceVBO;
   glBindBuffer(GL_ARRAY_BUFFER, instanceVBO);
   glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * ROWS*COLUMNS, colors, GL_STATIC_DRAW);
   glBindBuffer(GL_ARRAY_BUFFER, 0);
   glEnableVertexAttribArray(1);
   glBindBuffer(GL_ARRAY_BUFFER, instanceVBO);
   glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
   glVertexAttribDivisor(1, 1);

   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   glm::mat4 projection(0);
   projection[0][0] = 1.0f;
   projection[1][1] = 1.0f;
   projection[2][2] = 1.0f;
   projection[3][3] = 1.0f;
   setFrustum(projection, 45, 1, 5.0f, 1000.0f);
   // pass in the projection matrix
   projectionBuffer = glGetUniformLocation(myShaderProgram, "proj");
   glUniformMatrix4fv(projectionBuffer, 1, GL_FALSE, (const GLfloat *)&projection);

   glMatrixMode(GL_MODELVIEW);
   // pass in the modelView matrix
   glm::mat4 view(0);
   view[0][0] = 1.0f;
   view[1][1] = 1.0f;
   view[2][2] = 1.0f;
   view[3][3] = 1.0f;
   viewBuffer = glGetUniformLocation(myShaderProgram, "view");
   glUniformMatrix4fv(viewBuffer, 1, GL_FALSE, (const GLfloat *)&view);

}

// Function to check if two spheres centered at (x1,y1,z1) and (x2,y2,z2) with
// radius r1 and r2 intersect.
int checkSpheresIntersection(const float x1, const float y1, const float z1, const float r1, 
						       const float x2, const float y2, const float z2, const float r2)
{
   return ( (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2) + (z1-z2)*(z1-z2) <= (r1+r2)*(r1+r2) );
}

// Function to check if the spacecraft collides with an asteroid when the center of the base
// of the craft is at (x, 0, z) and it is aligned at an angle a to to the -z direction.
// Collision detection is approximate as instead of the spacecraft we use a bounding sphere.
int asteroidCraftCollision( const float x, const float z, const float a)
{
   int i,j;

   // Check for collision with each asteroid.
   int iMin = (int)(-1 * zVal / 30.0f) - 2; // all asteroids are size 30 apart
   int jMin = (int)((xVal / 30.0f) + COLUMNS / 2) - 2;
   if (iMin < 0) iMin = 0;
   if (jMin < 0) jMin = 0;
   if (iMin > ROWS) iMin = ROWS - 20;
   if (jMin > COLUMNS) jMin = COLUMNS - 20;

   for (i = iMin; i<iMin + 4; i++)
	   for (j = jMin; j<jMin + 4; j++)
		 if (arrayAsteroids[i][j].radius > 0 ) // If asteroid exists.
            if ( checkSpheresIntersection( x - 5.0f * sin( (PI/180.0f) * a), 0.0f, 
		         z - 5.0f * cos( (PI/180.0f) * a), 7.072f, 
		         arrayAsteroids[i][j].centerX, arrayAsteroids[i][j].centerY, 
		         arrayAsteroids[i][j].centerZ, arrayAsteroids[i][j].radius ) )
		       return 1;
   return 0;
}

// function taken from glu
void lookAt(glm::mat4 &m, float eyex, float eyey, float eyez, float centerx,
float centery, float centerz, float upx, float upy, float upz)
{
	glm::vec3 forward, side, up;

	// create identity matrix
	for (int i = 0; i < 4; i++) {
		m[i][0] = 0;
		m[i][1] = 0;
		m[i][2] = 0;
		m[i][3] = 0;
	}

	m[0][0] = 1.0f;
	m[1][1] = 1.0f;
	m[2][2] = 1.0f;
	m[3][3] = 1.0f;

	// glu code for lookat
	forward.x = centerx - eyex;
	forward.y = centery - eyey;
	forward.z = centerz - eyez;

	up.x = upx;
	up.y = upy;
	up.z = upz;

	forward = glm::normalize(forward);

	// Side = forward x up 
	side = glm::cross(forward, up);
	side = glm::normalize(side);

	// Recompute up as: up = side x forward 
	up = glm::cross(side, forward);

	m[0][0] = side[0];
	m[1][0] = side[1];
	m[2][0] = side[2];

	m[0][1] = up[0];
	m[1][1] = up[1];
	m[2][1] = up[2];

	m[0][2] = -forward[0];
	m[1][2] = -forward[1];
	m[2][2] = -forward[2];

	m = glm::translate(m, glm::vec3(-eyex, -eyey, -eyez));
}

// Drawing routine.
void drawScene(void)
{ 
   glClear (GL_COLOR_BUFFER_BIT| GL_DEPTH_BUFFER_BIT);

   glEnable(GL_DEPTH_TEST);

   glEnable(GL_DEPTH_RANGE);

   glEnable(GL_CULL_FACE);

   glCullFace(GL_BACK);

   // Use the buffer and shader for each circle.
   glUseProgram(myShaderProgram);
   glBindBuffer(GL_ARRAY_BUFFER, instancedBuffer);

   // Initialize the vertex position attribute from the vertex shader.
   GLuint loc = glGetAttribLocation(myShaderProgram, "vPosition");
   glEnableVertexAttribArray(loc);
   glVertexAttribPointer(loc, 3, GL_FLOAT, GL_FALSE, 0, 0);
   // copy over global points array in case anything has changed
   glBindBuffer(GL_ARRAY_BUFFER, instancedBuffer);
   glBufferData(GL_ARRAY_BUFFER, sizeof(points), points, GL_STATIC_DRAW);
   
   int iMin, partOfColumn;

   // Begin right viewport.
   glViewport(0, 0, width, height);
   glLoadIdentity();

   // Write text in isolated (i.e., before gluLookAt) translate block.
   // DOES NOT WORK WITHOUT GLUT
   //glPushMatrix();
   //glColor3f(1.0, 1.0, 1.0);
   //glRasterPos3f(5.0, 25.0, -30.0);
   //if (isFrustumCulled)  writeBitmapString((void*)font, "Frustum culling on.");
   //else writeBitmapString((void*)font, "Frustum culling off.");
   //glColor3f(1.0, 0.0, 0.0);
   //glRasterPos3f(-28.0, 25.0, -30.0);
   //if (isCollision)  writeBitmapString((void*)font, "Cannot - will crash!");
   //glPopMatrix();


   // Locate the camera at the tip of the cone and pointing in the direction of the cone.
   glm::mat4 view2(0);

   lookAt(view2, xVal - 10.0f * sin( (PI/180.0f) * angle), 
	         0.0f, 
			 zVal - 10.0f * cos( (PI/180.0f) * angle), 
	         xVal - 11.0f * sin( (PI/180.0f) * angle),
			 0.0f,
             zVal - 11.0f * cos( (PI/180.0f) * angle), 
             0.0f, 
			 1.0f, 
			 0.0f);
   glUniformMatrix4fv(viewBuffer, 1, GL_FALSE, (const GLfloat *)&view2);

   // Draw larger area of asteroids around the "spacecraft"
   // assume we don't care which direction the craft is facing
   // could do better than this if we know
   iMin = (int)(-1*zVal / 30.0f) - 50; // all asteroids are size 30 apart
   partOfColumn = (int)((xVal / 30.0f) + COLUMNS / 2) - 50; 
   if (iMin < 0) iMin = 0;
   if (partOfColumn < 0) partOfColumn = 0;
   if (iMin > ROWS) iMin = ROWS - 20;
   if (partOfColumn > COLUMNS) partOfColumn = COLUMNS - 20;
   glPolygonMode(GL_FRONT, GL_LINE);
   glPolygonMode(GL_BACK, GL_LINE);
   for (int i = iMin; i < iMin + 50; i++) {
	   glDrawArraysInstancedBaseInstance(GL_TRIANGLE_FAN, sphere_index, SPHERE_VERTEX_COUNT,
		  50+50, i*COLUMNS+partOfColumn);
   }
   // Turn off wireframe mode
   glPolygonMode(GL_FRONT, GL_FILL);
   glPolygonMode(GL_BACK, GL_FILL);

   // End right viewport.

   // Begin left viewport.
   glScissor(0, height / 1.25, width / 4.0, height / 4.0);
   glEnable(GL_SCISSOR_TEST);
   glClear(GL_DEPTH_BUFFER_BIT);
   glViewport(0, height / 1.25, width / 4.0, height / 4.0);
   glLoadIdentity();
   // Fixed camera 
   glm::mat4 view(0);
   view[0][0] = 1.0f;
   view[1][1] = 1.0f;
   view[2][2] = 1.0f;
   view[3][3] = 1.0f;
   glUniformMatrix4fv(viewBuffer, 1, GL_FALSE, (const GLfloat *)&view);

   // draw the line in the middle to separate the two viewports
   glPushMatrix();
   glLineWidth(2.0);
   glDrawArraysInstancedBaseInstance(GL_LINE_STRIP, line_index, LINE_VERTEX_COUNT, 1, 0);
   glLineWidth(1.0);
   glPopMatrix();

   // Write text in isolated (i.e., before gluLookAt) translate block.
   // DOES NOT WORK WITHOUT GLUT 
   //glPushMatrix();
   //glColor3f(1.0, 1.0, 1.0);
   //glRasterPos3f(5.0, 25.0, -30.0);
   //if (isFrustumCulled) writeBitmapString((void*)font, "Frustum culling on!");
   //else writeBitmapString((void*)font, "Frustum culling off!");
   //glColor3f(1.0, 0.0, 0.0);
   //glRasterPos3f(-28.0, 25.0, -30.0);
   //if (isCollision) writeBitmapString((void*)font, "Cannot - will crash!");
   //glPopMatrix();

   // Top Camera
   lookAt(view, xVal - 10 * sin((PI / 180.0) * angle),
	   90.0,
	   zVal - 10 * cos((PI / 180.0) * angle),
	   xVal - 11 * sin((PI / 180.0) * angle),
	   0.0,
	   zVal - 11 * cos((PI / 180.0) * angle),
	   0.0,
	   1.0,
	   0.0);
   glUniformMatrix4fv(viewBuffer, 1, GL_FALSE, (const GLfloat *)&view);

   // fixed view to draw around. kind of boring
   glPolygonMode(GL_FRONT, GL_LINE);
   glPolygonMode(GL_BACK, GL_LINE);

   iMin = 0; // start looking at the first row
   partOfColumn = COLUMNS / 2 - 15; // look at the middle of the row
   // draw the first 20 rows with only the part of the column around where the camera is
   // because the camera is fixed
   for (int i = 0; i < 50; i++) {
	   glDrawArraysInstancedBaseInstance(GL_TRIANGLE_FAN, sphere_index, SPHERE_VERTEX_COUNT,
		   50 + 50, i*COLUMNS + partOfColumn);
   }
   glPolygonMode(GL_FRONT, GL_FILL);
   glPolygonMode(GL_BACK, GL_FILL);

   // spacecraft moves and so we translate/rotate according to the movement
   glPushMatrix();
   glTranslatef(xVal, 0, zVal);
   glRotatef(angle, 0.0, 1.0, 0.0);

   glPushMatrix();
   glRotatef(-90.0, 1.0, 0.0, 0.0); // To make the spacecraft point down the $z$-axis initially.

   // Turn on wireframe mode
   glPolygonMode(GL_FRONT, GL_LINE);
   glPolygonMode(GL_BACK, GL_LINE);
   glDrawArraysInstancedBaseInstance(GL_TRIANGLE_FAN, cone_index, CONE_VERTEX_COUNT, 1, 1);
   // Turn off wireframe mode
   glPolygonMode(GL_FRONT, GL_FILL);
   glPolygonMode(GL_BACK, GL_FILL);
   glPopMatrix();
   glDisable(GL_SCISSOR_TEST);
   // End left viewport.
}

void keyInput(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	float tempxVal = xVal, tempzVal = zVal, tempAngle = angle;
	switch (key) {
	case GLFW_KEY_ESCAPE:
		// this will need to be fixed - not a proper exit
		exit(0);
		break;
	case GLFW_KEY_SPACE:
		// recreate the asteroid field
		// fill rate for asteroids is random out of 100%
		createAsteroidField(rand() % 100);
		break;
	default:
		break;
	}

	if (key >= 0 && key < 1024)
	{
		if (action == GLFW_PRESS)
			im.keys[key] = GL_TRUE;
		else if (action == GLFW_RELEASE)
		{
			im.keys[key] = GL_FALSE;
			im.keysProcessed[key] = GL_FALSE;
		}
	}

	//// function does not pass in multiple key pressed in a row, 
	//// so we have to get these manually
	//// even then, need to FIX to take into account GLFW_REPEAT
	//int leftPressed = glfwGetKey(window, GLFW_KEY_A) | glfwGetKey(window, GLFW_KEY_LEFT);
	//int rightPressed = glfwGetKey(window, GLFW_KEY_D) | glfwGetKey(window, GLFW_KEY_RIGHT);
	//int forwardPressed = glfwGetKey(window, GLFW_KEY_W) | glfwGetKey(window, GLFW_KEY_UP);
	//int backwardPressed = glfwGetKey(window, GLFW_KEY_S) | glfwGetKey(window, GLFW_KEY_DOWN);

	//// can't go both left and right
	//if (leftPressed) {
	//	//tempAngle = angle + 5.0f;
	//}
	//if (rightPressed) {
	//	tempAngle = angle - 5.0f;
	//}
	//// Angle correction.
	//if (tempAngle > 360.0) tempAngle -= 360.0f;
	//if (tempAngle < 0.0) tempAngle += 360.0f;


	//// can't go both forward and backwards
	//if (forwardPressed) {
	//	tempxVal = xVal - sin(angle * PI / 180.0f);
	//	tempzVal = zVal - cos(angle * PI / 180.0f);
	//}
	//if (backwardPressed) {
	//	tempxVal = xVal + sin(angle * PI / 180.0f);
	//	tempzVal = zVal + cos(angle * PI / 180.0f);
	//}

	//// Move spacecraft to next position only if there will not be collision with an asteroid.
	//if (!asteroidCraftCollision(tempxVal, tempzVal, tempAngle))
	//{
	//	isCollision = 0;
	//	angle = tempAngle;
	//	xVal = tempxVal;
	//	zVal = tempzVal;
	//}
	//else isCollision = 1;
}

// Routine to output interaction instructions to the C++ window.
void printInteraction(void)
{
   cout << "ALERT: The OpenGL window may take a while to come up because" << endl
		<< "of the time to build the quadtree!" << endl
		<<  endl;
   cout << "Interaction:" << endl;
   cout << "Press the left/right arrow keys to turn the craft." << endl
        << "Press the up/down arrow keys to move the craft." << endl
		<< "Press space to toggle between frustum culling enabled and disabled." << endl;
}

// Main routine.
int main(int argc, char **argv) 
{

	srand((unsigned)time(0));
	printInteraction();

	// set up the window
	GLFWwindow* window;

	// Initialize the library 
	if (!glfwInit())
		return -1;

	// Create a windowed mode window and its OpenGL context 
	window = glfwCreateWindow(WINDOW_X, WINDOW_Y, "spaceTravelFrustumCulled.cpp", nullptr, nullptr);
	resize(window, WINDOW_X, WINDOW_Y);
	glfwSetWindowSizeCallback(window, resize);
	glfwSetKeyCallback(window, keyInput);
	if (!window)
	{
		glfwTerminate();
		return -1;
	}

	// Make the window's context current 
	glfwMakeContextCurrent(window);

	// Init GLEW 
	glewInit();

	// init the graphics and rest of the app
	setup();

	// run!
	while (!glfwWindowShouldClose(window))
	{
		drawScene();
		im.handleInputs();
		glfwSwapBuffers(window);

		// Poll for and process events 
		glfwPollEvents();
	}

	glfwTerminate();

	return 0;

}

